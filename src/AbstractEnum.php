<?php

declare(strict_types=1);

namespace DNC\Enum;

abstract class AbstractEnum
{
    /**
     * @var static[][]
     */
    protected static array $instances;

    /**
     * @var mixed[]
     */
    protected static array $values;

    /**
     * @return int[]|string[]
     */
    abstract public static function getValues(): array;

    /**
     * @return int|string
     */
    abstract public function getValue();

    /**
     * @param \DNC\Enum\AbstractEnum|null $value
     *
     * @return bool
     */
    final public function is(?AbstractEnum $value): bool
    {
        if ($value === null) {
            return false;
        }

        if ($value instanceof static && $this->getValue() === $value->getValue()) {
            return true;
        }

        return false;
    }
}
