<?php

declare(strict_types=1);

namespace Tests\Examples;

use DNC\Enum\Type\StringEnum;

class StringStatus extends StringEnum
{
    public const PENDING  = 'pending';
    public const COMPLETE = 'complete';

    /**
     * @return array
     */
    public static function getValues(): array
    {
        return [
            self::PENDING,
            self::COMPLETE,
        ];
    }

    /**
     * @return static
     */
    public static function pending(): self
    {
        return self::make(self::PENDING);
    }

    /**
     * @return static
     */
    public static function complete(): self
    {
        return self::make(self::COMPLETE);
    }
}
