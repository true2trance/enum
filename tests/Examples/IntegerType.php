<?php

declare(strict_types=1);

namespace Tests\Examples;

use DNC\Enum\Type\IntegerEnum;

class IntegerType extends IntegerEnum
{
    public const USER  = 0;
    public const ADMIN = 1;

    /**
     * @return array
     */
    public static function getValues(): array
    {
        return [
            'user'  => self::USER,
            'admin' => self::ADMIN,
        ];
    }

    /**
     * @return static
     */
    public static function user(): self
    {
        return self::make(self::USER);
    }

    /**
     * @return static
     */
    public static function admin(): self
    {
        return self::make(self::ADMIN);
    }
}
