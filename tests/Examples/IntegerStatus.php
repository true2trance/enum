<?php

declare(strict_types=1);

namespace Tests\Examples;

use DNC\Enum\Type\IntegerEnum;

class IntegerStatus extends IntegerEnum
{
    public const PENDING  = 0;
    public const COMPLETE = 1;

    /**
     * @return array
     */
    public static function getValues(): array
    {
        return [
            'pending'  => self::PENDING,
            'complete' => self::COMPLETE,
        ];
    }

    /**
     * @return static
     */
    public static function pending(): self
    {
        return self::make(self::PENDING);
    }

    /**
     * @return static
     */
    public static function complete(): self
    {
        return self::make(self::COMPLETE);
    }
}
