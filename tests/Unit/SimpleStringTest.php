<?php

declare(strict_types=1);

namespace Tests\Unit;

use DNC\Enum\InvalidValueException;
use PHPUnit\Framework\TestCase;
use Tests\Examples\StringStatus;
use Tests\Examples\StringType;

class SimpleStringTest extends TestCase
{
    /**
     * @return void
     */
    public function testCreateSuccessfully(): void
    {
        $this->assertIsObject(StringStatus::pending());
        $this->assertIsObject(StringStatus::complete());
        $this->assertIsObject(StringType::user());
        $this->assertIsObject(StringType::admin());
    }

    /**
     * @return void
     */
    public function testCreateInvalidValueException(): void
    {
        $this->expectException(InvalidValueException::class);
        StringStatus::make('invalid');
    }

    /**
     * @return void
     */
    public function testSameClassEquals(): void
    {
        $int1 = StringStatus::pending();
        $int2 = StringStatus::pending();

        $this->assertEquals($int1, $int2);

        $this->assertTrue($int1->is($int2));
        $this->assertTrue($int2->is($int1));
    }

    /**
     * @return void
     */
    public function testSameClassNotEquals(): void
    {
        $int1 = StringStatus::pending();
        $int2 = StringStatus::complete();

        $this->assertNotEquals($int1, $int2);


        $this->assertFalse($int1->is($int2));
        $this->assertFalse($int2->is($int1));
    }

    /**
     * @return void
     */
    public function testDifferentClassNotEquals(): void
    {
        $int1 = StringStatus::make('pending');
        $int2 = StringType::make('user');

        $this->assertNotEquals($int1, $int2);

        $this->assertFalse($int1->is($int2));
        $this->assertFalse($int2->is($int1));
    }

    /**
     * @return void
     */
    public function testValueEquals(): void
    {
        $value = 'pending';

        $int = StringStatus::make($value);

        $this->assertEquals($int->getValue(), $value);
    }
}
