<?php

declare(strict_types=1);

namespace Tests\Unit;

use DNC\Enum\InvalidNameException;
use DNC\Enum\InvalidValueException;
use PHPUnit\Framework\TestCase;
use Tests\Examples\IntegerStatus;
use Tests\Examples\IntegerType;

class SimpleIntegerTest extends TestCase
{
    /**
     * @return void
     */
    public function testCreateSuccessfully(): void
    {
        $this->assertIsObject(IntegerStatus::pending());
        $this->assertIsObject(IntegerStatus::complete());
        $this->assertIsObject(IntegerType::user());
        $this->assertIsObject(IntegerType::admin());
    }

    /**
     * @return void
     */
    public function testCreateInvalidValueException(): void
    {
        $this->expectException(InvalidValueException::class);
        IntegerStatus::make(2);
    }

    /**
     * @return void
     */
    public function testSameClassEquals(): void
    {
        $int1 = IntegerStatus::pending();
        $int2 = IntegerStatus::pending();

        $this->assertEquals($int1, $int2);

        $this->assertTrue($int1->is($int2));
        $this->assertTrue($int2->is($int1));
    }

    /**
     * @return void
     */
    public function testSameClassNotEquals(): void
    {
        $int1 = IntegerStatus::pending();
        $int2 = IntegerStatus::complete();

        $this->assertNotEquals($int1, $int2);


        $this->assertFalse($int1->is($int2));
        $this->assertFalse($int2->is($int1));
    }

    /**
     * @return void
     */
    public function testDifferentClassNotEquals(): void
    {
        $int1 = IntegerStatus::make(0);
        $int2 = IntegerType::make(0);

        $this->assertNotEquals($int1, $int2);

        $this->assertFalse($int1->is($int2));
        $this->assertFalse($int2->is($int1));
    }

    /**
     * @return void
     */
    public function testValueEquals(): void
    {
        $value = 1;

        $int = IntegerStatus::make($value);

        $this->assertEquals($int->getValue(), $value);
    }

    /**
     * @return void
     */
    public function testCreateFromName(): void
    {
        $this->assertIsObject(IntegerStatus::makeFromName('pending'));
    }

    /**
     * @return void
     */
    public function testCreateFromInvalidName(): void
    {
        $this->expectException(InvalidNameException::class);
        IntegerStatus::makeFromName('foo');
    }

    /**
     * @return void
     */
    public function testNameEquals(): void
    {
        $name = 'pending';
        $int  = IntegerStatus::makeFromName($name);

        $this->assertEquals($int->getName(), $name);
    }

    /**
     * @return void
     */
    public function testGetNames(): void
    {
        $names = IntegerStatus::getNames();

        $this->assertContains('pending', $names);
        $this->assertContains('complete', $names);
    }
}
